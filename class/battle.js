export default class Battle {
    constructor(hero, ennemy) {
        this.ennemy = ennemy;
        this.hero = hero;
        this.isPartyOver = false;
    }

    theBagarre() {
        while (!this.isPartyOver) {
            this.hero.attack(this.ennemy);
            console.log(this.ennemy);
            if (this.ennemy.isDead()) {
                this.isPartyOver = true;
                this.hero.addXp(2);
            } else {
                this.ennemy.attack(this.hero);
                console.log(this.hero)
                if (this.hero.isDead()) {
                    this.isPartyOver = true;
                    this.ennemy.addXp(2);
                }
            }
        }
    }
}
