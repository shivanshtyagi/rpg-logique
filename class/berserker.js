import Enemy from '/class/enemy.js';
export default class Berserker extends Enemy {
    constructor(name) {
        super(name);
        this.health *= 1.5;
    };
}
