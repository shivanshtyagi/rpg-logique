import Enemy from '/class/enemy.js';
export default class Assassin extends Enemy {
    constructor(name) {
        super(name);
        this.assassinat = 'assassinat';
    };

    attack(cible) {
        const newhealth = cible.getHealth() - this.hitStrength;
        this.hitStrength *= 1.1;
        cible.setHealth(newhealth);
    }
}
