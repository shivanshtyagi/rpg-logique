import Hero from '/class/enemy.js';

    export default class Human extends Hero {
        constructor(name) {
            super(name);
        };
        attack(cible) {
            let newhealth = cible.getHealth();
            if (cible.getFlyStatus() == 'ennemis volants') {
                newhealth -= this.hitStrength * 1.1; // Humans : +10% de hitStrength sur les ennemis au sol,
            } else {
                newhealth -= this.hitStrength / 1.1; //-10% de hitStrength sur les ennemis volants
            }
            cible.setHealth(newhealth);
        }
    }
    