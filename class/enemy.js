        export default class Enemy {
            constructor(name) {
                this.name = name;
                this.health = 60;
                this.hitStrength = 20;
                this.lvl = 1;
                this.xp = 0;
            };
            getFlyStatus() {
                return 'none'
            }
            getname() {
                return this.name;
            }
            setName(val) {
                this.name = val;
            }
            getHealth() {
                return this.health;
            }
            setHealth(health) {
                this.health = Math.floor(health);
            }
            attack(cible) {
                const newhealth = cible.getHealth() - this.hitStrength;
                cible.setHealth(newhealth);
                console.log(this.name + ' attaque')
        
            }
            isDead() {
                return this.health <= 0;
            }
            getLvl() {
                return this.lvl;
            }
            getXp() {
                return this.xp;
            }
            addXp(xp) {
                this.xp = xp; //param valeur cp.
                while (this.xp >= this.lvl + 9) {
                    this.lvl++;
                    this.xp -= 10;
                    this.hitStrength++;
                    this.health++;
                }
            }
            getHitStrength() {
                return this.getHitStrength;
            }
            setHitStrength() {
                hitStrength = this.hitStrength;
            }
        }
        