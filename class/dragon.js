import Enemy from '/class/enemy.js';
export default class Dragon extends Enemy {
    constructor(name) {
        super(name);
        this.flyStatus = 'initiate';
    };
    getFlyStatus() {
        return this.flyStatus;
    }
    attack(cible) {
        if (this.flyStatus == 'initiate') {
            console.log(this.name + ' initiate');
            this.attackSol2(cible);
            this.flyStatus = 'au sol';

        } else if (this.flyStatus == 'au sol') {
            console.log(this.name + ' je suis au sol')
            this.attackSol(cible); //status ennemis volant.
            this.flyStatus = 'ennemis volants';

        } else if (this.flyStatus == 'ennemis volants') {
            console.log(this.name + ' la mort vient du ciel');
            this.attackEnLair(cible); //status atterit
            this.flyStatus = 'atterit';

        } else if (this.flyStatus == 'atterit') {
            console.log(this.name + ' atterit');
            this.attackSol2(cible); // statut au sol
            this.flyStatus = 'au sol';
        }
    }
    attackSol(cible) {
        const newhealth = cible.getHealth() - this.hitStrength;
        cible.setHealth(newhealth);
        this.health /= 1.1;
    }
    attackSol2(cible) {
        const newhealth = cible.getHealth() - this.hitStrength;
        cible.setHealth(newhealth);

    }

    attackEnLair(cible) {
        const newhealth = cible.getHealth() - this.hitStrength * 1.1;
        cible.setHealth(newhealth);

        this.health *= 1.1;
    }

}
