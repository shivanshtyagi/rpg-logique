export default class Hero {
    constructor(name) {
        this.setName(name);
        this.health = 150;
        this.hitStrength = 10;
        this.lvl = 1;
        this.xp = 0;
    };
    setName(name) {
        this.name = name;
    }
    getHealth() {
        return this.health;
    }
    setHealth(health) {
        this.health = Math.floor(health);
    }
    attack(cible) {
        const newhealth = cible.getHealth() - this.hitStrength;
        cible.setHealth(newhealth);
        console.log(this.name + ' attaque')
        if (cible.getHealth() <= 0) {
            this.addXp();
        }
    }
    isDead() {
        return this.health <= 0;
    }
    getLvl() {
            return this.lvl;
        }
        // setLvl()
    getXp() {
        return this.xp;
    }
    addXp(xp) {
        this.xp += xp; //param valeur cp.
        while (this.xp >= this.lvl + 9) {
            this.lvl++;
            this.xp -= 10;
            this.hitStrength++;
            this.health++;
        }
    }
    getRace() {
        return this.race;
    }
    setRace(race) {
        this.race = race;
    }
    getHitStrength() {
        return this.getHitStrength;
    }
    setHitStrength() {
        hitStrength = this.hitStrength
    }
}
